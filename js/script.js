/*mini activity*/

let favoriteFood = 'Sushi';
console.log(favoriteFood);

let sum159 = 150 + 9;
console.log(sum159);

let sum900 = 100 * 9;
console.log(sum900);

let isActive = true;
console.log('Is the user active?' + " " + isActive);

let arrayResto = ['Don Galbi', 'Noodle Bar', 'Al Dente', 'Farm to Table', 'Afriques']
console.log(arrayResto);

let faveArtist = {
  firstName: 'Jim',
  LastName: 'Parsons',
  stageName: 'Sheldon Cooper',
  birthDay: 'March 24 1973',
  age: '48',
  bestTvshow: 'Big Bang Theory',
  bestSong: 'Soft Kitty',
  isActive: true
  
};
console.log(faveArtist);

let numQA = 40;
let numQB = 8;

function receive2(divide1,divide2){
	let divideAnswer = divide1/divide2;
	console.log(divideAnswer);
	return divideAnswer;
}

let quotient = receive2(40,8);


console.log('The result of the division is ' + quotient);

/*end of mini activity*/


let num1 = 5;
let num2 = 10;
let num3 = 4;
let num4 = 40;


// num1 = num1 + num4;
num1 += num4;
console.log(num1);

/*num2 = num2 + num4;*/
num2 += num4;
console.log(num2)

num1 *= 2;
console.log(num1);


let string1 = "Boston";
let string2 = " Celtics";

/*string1 = string1 + string2;*/

string1 += string2;
console.log(string1);

num1 = num1 - string1;
num1 -= string1;
console.log(num1);

let string3 = "Hello everyone";
let myArray = string3.split("", 3);
console.log(myArray)


let mdasResult = 1 + 2 - 3 * 4 / 5;
console.log(mdasResult);

/*
3*4 = 12
12/5 = 2.4
1+2 = 3
3-2.4 = 0.6
*/

let pemdasResult = 1 + (2-3) * (4/5)
console.log(pemdasResult);

/*
2-3 = -1
4/5 = 0.8
0.8 * - 1 = -0.8
1 + (-0.8) = 0.2 
*/

/*pre-fex incrementation (REVIEW)*/ 

let z = 1;
++z;
console.log(z);

/*post fix incrementation*/
z++;
console.log(z);
console.log(z++);
console.log(z);


console.log(z++);
console.log(z);

console.log(++z);


/*comparison operators - used to compare values*/
console.log (1 == 1);
console.log('1' == 1);

/*strict equality*/
console.log ('1' === 1);


console.log('apple' == 'apple');
let isSame = 55 == 55;
console.log (isSame);

console.log(0 == false); /*force coercion (REVIEW)*/ 
console.log(1 == true);
console.log(true == 'true');

console.log(true == '1');

/*strict equality checks both value and type*/
console.log (1 === '1');
console.log('Juan' === 'Juan')
console.log('Maria' === 'maria')

/*inequality operators (!=)*/

console.log('1' != 1);	/*false*/
/*'1' converted to number 1
1 converted into number 1
1 == 1
not INEQUAL
*/

console.log('James' != 'John')

console.log(1 != true);
/*with type conversion: true was converted to 1
but if "true" was converted into a number and results to NaN
1 is not equal to NaN
IT IS INEQUAL*/


/*strict inequality operator checks if the 2 operands have diff values and data types*/

console.log('5' !== 5) /*true*/
console.log(5 !== 5) /*false*/


let name1 = 'Juan';
let name2 = 'Maria';
let name3 = 'Pedro';
let name4 = 'Perla';

let number1 = 50;
let number2 = 60;
let numString1 = "50";
let numString2 = "60";

console.log(numString1 == number1); /*true*/
console.log(numString1 === number1); /*false*/
console.log(numString1 != number1); /*false*/
console.log(name4 !== name3); /*true*/
console.log(name1 == 'juan'); /*false*/
console.log(name1 === "Juan"); /*true*/

/*relational comparison operators*/

let x = 500;
let y = 700;
let w = 8000;
let numString3 = "5500";

console.log(x > y);
console.log(w > y);

console.log(w < y);
console.log(y < y);
console.log(x < 1000);
console.log(numString3 < 1000);
console.log(numString3 < 6000);
console.log(numString3 < "Juan"); /*logical error or erratic*/


/*logical operators*/
/*&& operator - both operands on the left and right or all must be true or false*/
/*T && T = true, T && F = false, F && F = false*/

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true; 

let authorization1 = isAdmin && isRegistered;
console.log(authorization1); /*false*/

let authorization2 = isLegalAge && isRegistered;
console.log(authorization2);

let authorization3 = isAdmin && isLegalAge;
console.log(authorization3);

let requiredLevel = 95;
let requiredAge = 18;

let authorization4 = isRegistered && requiredLevel === 25;
console.log(authorization4);
let authorization5 = isRegistered && isLegalAge && requiredLevel === 95;
console.log(authorization5);

let userName1 = 'gamer2021';
let userName2 = 'shadowmaster'
let userAge1 = 15;
let userAge2 = 30;

let registration1 = userName1.length > 9 && userAge1 >= requiredAge;
console.log(registration1);

let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
console.log(registration2);

let registration3 = userName1.length > 8 && userAge2 >= requiredAge;
console.log(registration3);

/*OR operator
if there is at least one TRUE on the operands, it will result to TRUE.

T || T = T
T || F = T
F || T = T
F || F = F



*/

let userLevel1 = 100;
let userLevel2 = 65;

let guildRequirement = isRegistered && userLevel1 >= requiredLevel && userAge1 >= requiredAge;
console.log(guildRequirement);

let guildRequirement0 = isRegistered || userLevel1 >= requiredLevel || userAge1 >= requiredAge;
console.log(guildRequirement0);

let guildRequirement2 = userLevel1 >= requiredLevel || userAge1 >= requiredAge;
console.log(guildRequirement2);

let guildAdmin = isAdmin  || userLevel2 >= requiredLevel;
console.log(guildAdmin);

/*not operator = (!) 

 reverses the value of a boolean result
*/

let guildAdmin1 = !isAdmin  || userLevel2 >= requiredLevel;
console.log(guildAdmin1);

let opposite1 = !isAdmin;
let opposite2 = !isLegalAge;

console.log(opposite1);
console.log(opposite2);


/*if - if statements will perform the code if the condition results a true statement*/

const candy = 100;
if (candy >= 100){

  console.log('You got a cavity!')
}

let userName3 = "crusader_1993";
let userLevel3 = 25;
let userAge3 = 30;

if(userName3.length > 10){

  console.log('Welcome to the game!');
}

/*else statement*/
if(userName3 >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge){
    console.log('Thank you for joining Noobies Guild')
  }
  else {
    console.log("You're too strong to be a noob.");
  }


/*else if*/

if(userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge)
{
  console.log('Thank you for joining the Noobies Guild.');
}
  else if(userLevel3 > 25){
    console.log("You're still too strong!"); 
}
  else if(userAge3 < requiredAge){
    console.log("You're too young to join the guild.")
  }
  else {
    console.log("End of the condition.");
  }


  /*if-else in a function*/
  function addNum(num1, num2){
    if(typeof num1 === "number" && typeof num2 === "number"){
      console.log('Run only if both arguments passed are number types.');
      console.log(num1 + num2);
   } else {;
    console.log("One or both the arguments are not numbers.")
   };
  };



/*  let customerName = prompt('Enter your name:')
  if(customerName != null){
    document.getElementById("welcome").innerHTML = "Hello" + customerName;

  }
*/

  addNum(5, 2)


/*MINI ACTIVITY*/


  function login(username, password){
    if(typeof username === "string" && typeof password === "string"){
      console.log('Both arguments are strings.');
    } 
    
    else if(password.length >= 8)
    {
      console.log("Password is too short.");
    } 

    else if(username.length >= 8 && password.length >= 8)
    {
      console.log("Credentials are too short.")
    }

    else if(username.length >= 8)
    {
      console.log("Username is too short.")
    }

    else if(password.length >= 8)
    {
      console.log("Password is too short.");
    } 
  }